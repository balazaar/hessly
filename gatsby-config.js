const languages = require('./src/data/languages');

module.exports = {
  siteMetadata: {
    title: `Hessly Sh.P.K`,
    description: `
    Hessly Sh.P.K u themelua në vitin 2010 me veprimtari prodhuese dhe tregtuese në sektorin e materialeve të ndërtimit të thatë. Fillimet tona janë modeste por qëndrimi dhe profesionalizmi ynë na kanë ndihmuar për tu konsoliduar si një nga prodhuesit kryesorë të profileve të sistemit të thatë në tregun Shqiptar. Kjo këmbëngulje na ka dhënë gjithashtu mundësinë e zgjerimit në sektorë të tjerë të prodhimit dhe tregtimit. Hessly Sh.P.K, prej disa vitesh, ka zgjeruar kapacitetet e biznesit në prodhimin dhe tregtimin e mbulesave të lyera të çelikut dhe gjithashtu në sektorin e tregtimit të rafteve metalikë të supermarketeve dhe aksesorëve të fushës. Kapaciteti i sotëm prodhues i përpunimit të profileve të sistemit të thatë është 3000 ton/vit me një kapacitet të shtuar prej 1000 ton/vit në fushën e prodhimit të mbulesave të lyera të llamarinës.
    Ne jemi krenarë për efiçencën që tregojmë në marrëdheniet tona me tregun e cila bëhet e mundur nga një staf tejet i kualifikuar në sektorin e prodhimit dhe menaxhimit. Tre vlerat tona kryesore në marrëdhëniet e përditshme të të bërit biznes janë: Cilësia, Efiçenca dhe Besueshmëria.
    Cilësia: Hessly ka një historik të gjatë të përmbushjes së nevojave të klientëve sipas kërkesave specifike duke ruajtur gjithmonë një cilësi të qëndrueshme në sajë të ndjekjes së një etike themelore të të bërit biznes.
    Efiçenca: Sigurimi i një procesi prodhimi të shpejtë i cili përmbush çdo kërkesë në kohë rekord.
    Besueshmëria: Ngritja e marrëdhënieve afatgjata janë pjesë themelore e punës së Hessly. Ne jemi gjithmonë të hapur dhe tëpërkushtuar për të ofruar ndihmë dhe zgjidhur çdo problem, duke i ofruar çdo klienti një eksperiencë tërësisht profesionale.
    Vizioni ynë ndërthuret me konsolidimin tonë si lider në secilin sektor që operojmë në tregun shqiptar, duke shpresuar gjithashtu, që në një të ardhme afat-gjatë të pozicionohemi si një nga furnitorët kryesorë në rajonin e Ballkanit.
`,
    siteUrl: 'https://hessly.al',
    image: 'img.jpg',
    author: {
      name: 'Hessly',
    },
    organization: {
      name: 'Hessly Sh.P.K',
      url: 'https://hessly.al',
      logo: 'img/logo.svg',
    },
    social: {
      twitter: '@twitter',
      fbAppID: '',
    },
    languages,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: 'gatsby-plugin-sass',
      options: {
        sassOptions: {
          indentedSyntax: true,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-i18n',
      options: {
        langKeyForNull: 'any',
        langKeyDefault: 'sq',
        useLangKeyLayout: false,
      },
    },
    {
      resolve: 'gatsby-plugin-i18n-tags',
      options: {
        // Default options
        tagPage: 'src/templates/tags.jsx',
        tagsUrl: '/tags/',
        langKeyForNull: 'any',
      },
    },
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/data/articles`,
      },
    },
    {
      // keep as first gatsby-source-filesystem plugin for gatsby image support
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/static/img`,
        name: 'uploads',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/img`,
        name: 'images',
      },
    },
    'gatsby-transformer-javascript-frontmatter',
    `gatsby-plugin-image`,
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          {
            resolve: 'gatsby-remark-relative-images',
            options: {
              name: 'uploads',
            },
          },
          {
            resolve: 'gatsby-remark-images',
            options: {
              // It's important to specify the maxWidth (in pixels) of
              // the content container as this plugin uses this as the
              // base for generating different widths of each image.
              maxWidth: 2048,
            },
          },
          {
            resolve: 'gatsby-remark-copy-linked-files',
            options: {
              destinationDir: 'static',
            },
          },
        ],
      },
    },
    `gatsby-plugin-netlify-cms`,
    `gatsby-plugin-netlify`,
    {
      resolve: 'gatsby-plugin-purgecss', // purges all unused/unreferenced css rules
      options: {
        develop: true, // Activates purging in npm run develop
        purgeOnly: ['/all.sass'], // applies purging only on the bulma css file
      },
    }, // must be after other CSS plugins
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        output: `/sitemap.xml`,
      },
    },
    'gatsby-plugin-robots-txt',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Hessly Sh.P.K`,
        short_name: `Hessly`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#D64000`,
        display: `standalone`,
        icon: `src/img/logo.png`,
      },
    },
    {
      resolve: 'gatsby-plugin-react-leaflet',
      options: {
        linkStyles: true, // (default: true) Enable/disable loading stylesheets via CDN
      },
    },
    `gatsby-plugin-offline`,
  ],
};
