module.exports = {
  interiorExterior: [
    '/en/products/interior-exterior',
    '/sq/produkte/interior-exterior',
  ],
  drywallProfiles: ['/en/products/drywall-profiles-accessories', '/sq/produkte/profile-aksesore'],
  steelRoofing: ['/en/products/steel-roofing', '/sq/produkte/mbulesa-celiku-llamarine'],
  ceilingSystems: ['/en/products/hessly-ceiling-systems', '/sq/produkte/hessly-sisteme-gipsi-tavani'],
  shelves: [
    '/en/products/shelves-accessories',
    '/sq/produkte/rafte-aksesore',
  ],
};
