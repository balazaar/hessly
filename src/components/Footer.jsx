import React from 'react';
import { Link } from 'gatsby';
import logo from '../img/logo.png';
import select from '../components/utils';
import { FormattedMessage } from 'react-intl';
import menuTree from '../data/menuTree';
import {
  FaFacebook,
  // FaTwitter,
  FaInstagram,
  FaLinkedin,
} from 'react-icons/fa';
import Copyright from '../components/Copyright';
import ScrollToTop from '../components/ScrollToTop';

const Footer = class extends React.Component {
  render() {
    const props = this.props;
    const sel = select(props.langKey);
    return (
      <footer className="footer has-background-black has-text-white-ter">
        <div className="content has-text-centered has-background-black has-text-white-ter">
          <div className="container has-background-black has-text-white-ter">
            <div className="columns">
              <div className="column is-one-quarter">
                <section className='menu'>
                  <ul className="menu-list">
                    <Link to="/" className="navbar-item" title="Logo">
                      <img src={logo} alt="Hessly" style={{ width: '110px' }} />
                    </Link>
                  </ul>
                </section>
              </div>
              <div className="column is-4">
                <section className="menu">
                  <ul className="menu-list">
                    <li className='navbar-item'
                      style={{ fontWeight: 'bold' }}>
                      Links
                    </li>
                    <li>
                      <Link to={'/' + props.langKey} className="navbar-item">
                        <FormattedMessage id="home" />
                      </Link>
                    </li>
                    <li>
                      <Link
                        className="navbar-item"
                        to={
                          '/' + props.langKey + '/' + menuTree.products[sel] + '/'
                        }
                      >
                        <FormattedMessage id="products" />
                      </Link>
                    </li>
                    <li>
                      <Link
                        className="navbar-item"
                        to={
                          '/' + props.langKey + '/' + menuTree.services[sel] + '/'
                        }
                      >
                        <FormattedMessage id="services" />
                      </Link>
                    </li>
                    <li>
                      <Link
                        className="navbar-item"
                        to={
                          '/' + props.langKey + '/' + menuTree.about[sel] + '/'
                        }
                      >
                        <FormattedMessage id="about" />
                      </Link>
                    </li>
                    <li>
                      <Link
                        className="navbar-item"
                        to={
                          '/' +
                          props.langKey +
                          '/' +
                          menuTree.contact[sel] +
                          '/'
                        }
                      >
                        <FormattedMessage id="contact" />
                      </Link>
                    </li>
                  </ul>
                </section>
              </div>
              <div className="column is-4">
                <section className='menu'>
                  <ul className="menu-list">
                    <li className='navbar-item'
                      style={{ fontWeight: 'bold' }}>
                      <FormattedMessage id="address" />
                    </li>
                    <li className='navbar-item'><FormattedMessage id="address1" /></li>
                    <li className='navbar-item'><FormattedMessage id="address2" /></li>
                  </ul>
                </section>
              </div>
              <div className="column is-4 social">
                <section className='menu'>
                  <ul className="menu-list" style={{marginLeft: '0px'}}>
                    <li className='navbar-item'
                      style={{ fontWeight: 'bold', justifyContent: 'center' }}>
                      Social
                    </li>
                  </ul>
                    <a title="facebook" href="https://www.facebook.com/Hessly-Albania-109660723983037/">
                      <FaFacebook className="facebook-icon" size="2em" />
                    </a>
                    <a title="instagram" href="https://instagram.com/hessly.al">
                      <FaInstagram className="instagram-icon" size="2em" />
                    </a>
                    <a title="linkedin" href="https://www.linkedin.com/company/hessly-sh-p-k">
                      <FaLinkedin className="linkedin-icon" size="2em" />
                    </a>
                </section>
              </div>
            </div>
          </div>
          <Copyright />
        </div>
        {/* <ScrollToTop /> */}
      </footer>
    );
  }
};

export default Footer;
