import React from 'react';
import { FaMapMarkerAlt, FaPhone, FaRegEnvelope } from 'react-icons/fa';
import PropTypes from 'prop-types';
import CardSlide from '../components/CardSlide';
import { FormattedMessage } from 'react-intl';

const ContactDetails = ({ infos, address1, address2, image, phone1, phone2 }) => (
  <div className="section box">
    <div className="container">
      <h3 className="title">
        <FormattedMessage id="contact.infos" />
      </h3>
      <div className="columns is-vcentered">
        <div className="column">
          {/* <CardSlide
            style={{ maxWidth: '20%' }}
            imageInfo={image}
          /> */}
        </div>
        <div className="column is-vertical-center">
          {address1 && (
            <div className="content">
              <a
                className="Contact--Details--Item"
                href={`https://www.google.com.au/maps/search/${encodeURI(
                  address1
                )}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                <FaMapMarkerAlt className="menu-names" /> {address1}
              </a>
            </div>
          )}
          {phone1 && (
            <div className="content">
              <a className="Contact--Details--Item" href={`tel:${phone1}`}>
                <FaPhone className="menu-names" /> {phone1}
              </a>
            </div>
          )}
        </div>
      </div>
    </div>
  </div>
);

ContactDetails.propTypes = {
  infos: PropTypes.string,
  image: PropTypes.object,
  address: PropTypes.string,
  phone: PropTypes.string,
  email: PropTypes.string,
};

export default ContactDetails;
