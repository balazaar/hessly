import React from 'react';
import * as PropTypes from 'prop-types';
import { graphql, Link } from 'gatsby';
import Layout from '../components/Layout';
import SEO from '../components/SEO/SEO';
import Content, { HTMLContent } from '../components/Content';
import menuTree from '../data/menuTree';
import select from '../components/utils';
import { FormattedMessage } from 'react-intl';

const ProductCategoryTemplate = ({
  contentComponent,
  langKey,
  products
}) => {
  const PageContent = contentComponent || Content;
  const sel = select(langKey);
  return (
    <div className="container content grid-container">
        { products && 
          products.map((product, i) => {
           return <div key={i} class="card product-category">
            <div class="card-image">
              <figure class="image is-full">
              <img src={product.image.childImageSharp.fluid.src} alt={product.prodTitle} />
              </figure>
            </div>
            <div class="card-content">
              <div class="content">
                  <Link to={product.link}><h1>{product.prodTitle}</h1></Link>
              </div>
            </div>
          </div>
          })
        }
    </div>
  );
};

ProductCategoryTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
  langKey: PropTypes.string
};

class ProductCategory extends React.Component {
  render() {
    var dataMarkdown = [];
    let data;
    if (this.props.data !== null) {
      dataMarkdown = this.props.data.markdownRemark;
      data = this.props.data;
    }
    const jsonData = this.props.data.allArticlesJson.edges[0].node.articles;
    const { frontmatter } = data.markdownRemark;
    const langKey = frontmatter.lang;
    const products = frontmatter.products
    
    return (
      <Layout
        className="container"
        data={this.props.data}
        jsonData={jsonData}
        location={this.props.location}
      >
        <SEO frontmatter={frontmatter} />
        <div>
          <ProductCategoryTemplate
            contentComponent={HTMLContent}
            title={dataMarkdown.frontmatter.title}
            content={dataMarkdown.html}
            langKey={langKey}
            products={products}
          />
        </div>
      </Layout>
    );
  }
}

ProductCategory.propTypes = {
  data: PropTypes.object.isRequired,
};

export default ProductCategory;

export const pageQuery = graphql`
  query ProductCategoryQuery($id: String!) {
    site {
      siteMetadata {
        languages {
          defaultLangKey
          langs
        }
      }
    }
    allArticlesJson(filter: { title: { eq: "home" } }) {
      edges {
        node {
          articles {
            en
            sq
          }
        }
      }
    }
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        id
        title
        description
        lang
        products {
          prodTitle
          image {
            childImageSharp {
              fluid(maxWidth: 2048, quality: 100) {
                ...GatsbyImageSharpFluid
                src
              }
            }
          }
          link
        }
      }
      fields {
        slug
      }
    }
  }
`;
