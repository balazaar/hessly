import React from 'react';
import * as PropTypes from 'prop-types';
import { graphql, Link } from 'gatsby';
import Layout from '../components/Layout';
import SEO from '../components/SEO/SEO';
import Content, { HTMLContent } from '../components/Content';
import menuTree from '../data/menuTree';
import select from '../components/utils';
import { FormattedMessage } from 'react-intl';
import imageS1 from '../../static/img/DryWallService.jpeg';
import imageS2 from '../../static/img/RoofingService.jpeg';
import imageS3 from '../../static/img/ShelvingService.jpeg';

const ServicesPageTemplate = ({
  title,
  content,
  contentComponent,
  langKey,
  descriptionS1,
  descriptionS2,
  descriptionS3,
  titleS1,
  titleS2,
  titleS3
}) => {
  const PageContent = contentComponent || Content;
  const sel = select(langKey);
  return (
    <div className="container content service">
      <section className="section">
        <div className='services-flex'>
        <div className='column flex left'>
            <div className='column inner-container'>
              <h1>{titleS1}</h1>
              <p>{descriptionS1}</p>
              <Link
                  className="hbtn"
                  to={'/' + langKey + '/' + menuTree.contact[sel] + '/'}
                >
                  <FormattedMessage id='contact.contact-us'/>
              </Link>
            </div>
            <div
            className='column bg'
            style={{
              backgroundImage: `url(${imageS1})`,
              backgroundPosition: `center`,
              backgroundAttachment: `scroll`,
              backgroundSize: `cover`,
              padding: '8rem'
            }}>
            </div>
          </div>
          <div className='column flex left'>
          <div
            className='column bg'
            style={{
              backgroundImage: `url(${imageS2})`,
              backgroundPosition: `center`,
              backgroundAttachment: `scroll`,
              backgroundSize: `cover`,
              padding: '8rem'
            }}>
            </div>
          <div className='column inner-container'>
              <h1>{titleS2}</h1>
              <p>{descriptionS2}</p>
              <Link
                  className="hbtn"
                  to={'/' + langKey + '/' + menuTree.contact[sel] + '/'}
                >
                  <FormattedMessage id='contact.contact-us'/>
              </Link>
            </div>
          </div>

          <div className='column flex left'>
            <div className='column inner-container'>
              <h1>{titleS3}</h1>
              <p>{descriptionS3}</p>
              <Link
                  className="hbtn"
                  to={'/' + langKey + '/' + menuTree.contact[sel] + '/'}
                >
                  <FormattedMessage id='contact.contact-us'/>
              </Link>
            </div>
            <div
            className='column bg'
            style={{
              backgroundImage: `url(${imageS3})`,
              backgroundPosition: `center`,
              backgroundAttachment: `scroll`,
              backgroundSize: `cover`,
              padding: '8rem'
            }}>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

ServicesPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
  tags: PropTypes.array,
  langKey: PropTypes.string,
  imageS1: PropTypes.string
};

class ServicesPage extends React.Component {
  render() {
    var dataMarkdown = [];
    if (this.props.data !== null) {
      dataMarkdown = this.props.data.markdownRemark;
    }
    const jsonData = this.props.data.allArticlesJson.edges[0].node.articles;
    const { frontmatter } = dataMarkdown;

    //images
    // const imageS1 = frontmatter.imageS1.childImageSharp.gatsbyImageData.src;
    // console.log("im", imageS1)
    // const imageS2 = frontmatter.imageS2.childImageSharp.gatsbyImageData.src;
    // const imageS3 = frontmatter.imageS3.childImageSharp.gatsbyImageData.src;

    const langKey = frontmatter.lang;
    const tags = frontmatter.tags;
    const descriptionS1 = frontmatter.descriptionS1
    const descriptionS2 = frontmatter.descriptionS2
    const descriptionS3 = frontmatter.descriptionS3
    const titleS1 = frontmatter.titleS1
    const titleS2 = frontmatter.titleS2
    const titleS3 = frontmatter.titleS3
    return (
      <Layout
        className="container"
        data={this.props.data}
        jsonData={jsonData}
        location={this.props.location}
      >
        <SEO frontmatter={frontmatter} />
        <div>
          <ServicesPageTemplate
            contentComponent={HTMLContent}
            title={dataMarkdown.frontmatter.title}
            content={dataMarkdown.html}
            tags={tags}
            langKey={langKey}
            descriptionS1={descriptionS1}
            descriptionS2={descriptionS2}
            descriptionS3={descriptionS3}
            titleS1={titleS1}
            titleS2={titleS2}
            titleS3={titleS3}
          />
        </div>
      </Layout>
    );
  }
}

ServicesPage.propTypes = {
  data: PropTypes.object.isRequired,
};

export default ServicesPage;

export const pageQuery = graphql`
  query ServicesPageQuery($id: String!) {
    site {
      siteMetadata {
        languages {
          defaultLangKey
          langs
        }
      }
    }
    allArticlesJson(filter: { title: { eq: "home" } }) {
      edges {
        node {
          articles {
            en
            sq
          }
        }
      }
    }
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        id
        title
        description
        tags
        lang
        descriptionS1
        descriptionS2
        descriptionS3
        titleS1
        titleS2
        titleS3
      }
      fields {
        slug
      }
    }
  }
`;
