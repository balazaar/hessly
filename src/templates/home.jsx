import React from 'react';
import * as PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import Layout from '../components/Layout';
import SEO from '../components/SEO/SEO';
import Content, { HTMLContent } from '../components/Content';
import select from '../components/utils';
import { FormattedMessage } from 'react-intl';
import { FaGlobe, FaHandshake, FaRing } from 'react-icons/fa';
import cat1 from '../img/cat1image.jpeg';
import cat2 from '../img/cat2image.jpeg';
import cat3 from '../img/cat3image.jpeg';
import cat4 from '../img/cat4image.jpeg';
import cat5 from '../img/cat5image.jpeg';
import s1 from '../img/DryWallService.jpeg';
import s2 from '../img/RoofingService.jpeg';
import s3 from '../img/ShelvingService.jpeg';
const HomePageTemplate = ({
  imageCardSL,
  image,
  heading,
  display,
  array,
  mainpitch,
  main,
  testimonials,
  title,
  content,
  contentComponent,
  tags,
  langKey,
}) => {
  const PageContent = contentComponent || Content;

  return (
    <div>
      <div
        className="hero is-halfheight has-background-black"
        style={{
          backgroundImage: `url(${!!image.childImageSharp ? image.childImageSharp.fluid.src : image
            })`,
          backgroundPosition: `center`,
          backgroundAttachment: `scroll`,
          backgroundSize: `cover`,
        }}>
        <div
          style={{
            display: 'flex',
            height: '150px',
            lineHeight: '1',
            justifyContent: 'space-around',
            alignItems: 'left',
            flexDirection: 'column',
            marginTop: '13rem'
          }}
        >
          <h1
            className="is-centered hes-h1"
            style={{
              color: 'white',
              lineHeight: '1.5',
              padding: '0.25em',
              textAlign: `center`,
            }}
          >
            {title}
          </h1>
        </div>
        <div className="scroll-indicator">
          <div className="scroll-container">
            <div className="chevron"></div>
            <div className="chevron"></div>
            <div className="chevron"></div>
            <span className="text">{heading}</span>
          </div>
        </div>
      </div>
      <h2 className="main-content-pitch">
          <FormattedMessage id='highlight'/>
        </h2>
      <section className='highlights'>
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <FaRing size='4em'/>
            </figure>
          </div>
          <div class="card-content">
            <div class="media">
              <div class="media-content">
                <p class="title is-6"><FormattedMessage id='h1t' /></p>
              </div>
            </div>
            <div class="content">
              <FormattedMessage id='h1s' />
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <FaHandshake size='4em'/>
            </figure>
          </div>
          <div class="card-content">
            <div class="media">
              <div class="media-content">
                <p class="title is-6"><FormattedMessage id='h2t' /></p>
              </div>
            </div>
            <div class="content"><FormattedMessage id='h2s' />
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
              <FaGlobe size='4em' />
            </figure>
          </div>
          <div class="card-content">
            <div class="media">
              <div class="media-content">
                <p class="title is-6"><FormattedMessage id='h3t' /></p>
              </div>
            </div>
            <div class="content">
              <FormattedMessage id='h3s' />
            </div>
          </div>
        </div>
      </section>

      <h2 className="main-content-pitch">
          <FormattedMessage id='categories'/>
        </h2>
      <section className='highlights'>
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <img src={cat1} alt="Category 1" />
            </figure>
          </div>
          <div class="card-content">
            <div class="content">
              <FormattedMessage id='cat1' />
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <img src={cat2} alt="Category 1" />
            </figure>
          </div>
          <div class="card-content">
            <div class="content">
              <FormattedMessage id='cat2' />
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <img src={cat3} alt="Category 1" />
            </figure>
          </div>
          <div class="card-content">
            <div class="content">
              <FormattedMessage id='cat3' />
            </div>
          </div>
        </div>
      </section>
        <section className="highlights">
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <img src={cat4} alt="Category 1" />
            </figure>
          </div>
          <div class="card-content">
            <div class="content">
              <FormattedMessage id='cat4' />
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <img src={cat5} alt="Category 1" />
            </figure>
          </div>
          <div class="card-content">
            <div class="content">
              <FormattedMessage id='cat5' />
            </div>
          </div>
        </div>
        </section>
        <h2 className="main-content-pitch">
          <FormattedMessage id='services'/>
        </h2>
        <section className="highlights">
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <img src={s1} alt="Category 1" />
            </figure>
          </div>
          <div class="card-content">
            <div class="content">
              <FormattedMessage id='s1' />
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <img src={s2} alt="Category 1" />
            </figure>
          </div>
          <div class="card-content">
            <div class="content">
              <FormattedMessage id='s2' />
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-image">
            <figure class="image is-full">
            <img src={s3} alt="Category 1" />
            </figure>
          </div>
          <div class="card-content">
            <div class="content">
              <FormattedMessage id='s3' />
            </div>
          </div>
        </div>
        </section>
    </div>
  );
};

HomePageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  heading: PropTypes.string,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
  tags: PropTypes.array,
  langKey: PropTypes.string,
};

class HomePage extends React.Component {
  render() {
    let data;
    let dataMarkdown = [];
    if (this.props.data !== null) {
      dataMarkdown = this.props.data.markdownRemark;
      data = this.props.data;
    }
    const jsonData = data.allArticlesJson.edges[0].node.articles;
    const langKey = dataMarkdown.frontmatter.lang;
    const { frontmatter } = data.markdownRemark;
    const { display } = frontmatter.slider;
    const { array } = frontmatter.slider;
    const sel = select(langKey);
    const image = frontmatter.image.childImageSharp.fluid.src;
    const tags = frontmatter.tags;

    return (
      <Layout
        className="content home"
        data={this.props.data}
        jsonData={jsonData}
        location={this.props.location}
      >
        <SEO frontmatter={frontmatter} postImage={image} />
        <div>
          <HomePageTemplate
            imageCardSL={dataMarkdown.frontmatter.imageCardSL}
            image={dataMarkdown.frontmatter.image}
            heading={dataMarkdown.frontmatter.heading}
            display={display}
            array={array}
            mainpitch={dataMarkdown.frontmatter.mainpitch}
            main={dataMarkdown.frontmatter.main}
            testimonials={dataMarkdown.frontmatter.testimonials}
            contentComponent={HTMLContent}
            title={dataMarkdown.frontmatter.title}
            content={dataMarkdown.html}
            tags={tags}
            langKey={langKey}
          />
        </div>
      </Layout>
    );
  }
}

HomePage.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  data: PropTypes.object.isRequired,
};

export default HomePage;

export const pageQuery = graphql`
  query HomePageQuery($id: String!) {
    site {
      siteMetadata {
        languages {
          defaultLangKey
          langs
        }
      }
    }
    allArticlesJson(filter: { title: { eq: "home" } }) {
      edges {
        node {
          articles {
            en
            sq
          }
        }
      }
    }
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        id
        title
        description
        tags
        lang
        image {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
              src
            }
          }
        }
        heading
        mainpitch {
          heading
          subheading
          title
          description
          link
        }
        slider {
          display
          array {
            original
            thumbnail
            originalAlt
            originalTitle
            description
          }
        }
        imageCardSL {
          alt
          image {
            childImageSharp {
              gatsbyImageData(width: 128, quality: 84, layout: CONSTRAINED)
            }
          }
          name
          description
          website
        }
        main {
          image1 {
            alt
            image {
              childImageSharp {
                gatsbyImageData(width: 500, quality: 90, layout: CONSTRAINED)
              }
            }
          }
        }
        testimonials {
          author
          quote
        }
      }
      fields {
        slug
      }
    }
  }
`;
