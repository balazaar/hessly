import React from 'react';
import * as PropTypes from 'prop-types';
import { graphql, Link } from 'gatsby';
import Layout from '../components/Layout';
import SEO from '../components/SEO/SEO';
import Content, { HTMLContent } from '../components/Content';
import select from '../components/utils';
import Slider from '../components/Slider';
import Collapsible from 'react-collapsible';
import { FormattedMessage } from 'react-intl';

const PDPTemplate = ({
  contentComponent,
  langKey,
  title,
  array,
  display,
  prodDesc,
  application,
  specs
}) => {
  const PageContent = contentComponent || Content;
  const sel = select(langKey);
  return (
    <div className="pdp container flex">
      <div className="left-pdp">
        <Slider array={array} display={display} />

        <div className="collapsible-elements">
        <Collapsible trigger={<FormattedMessage id='specs'/>}>
          <article className='specs'>
            {
              specs.map((spec, i) => {
                return <li key={i}>{spec.spec}</li>
             })
            }
          </article>
          </Collapsible>

          {
            application && 
            <Collapsible trigger={<FormattedMessage id='application'/>}>
              <div className='application'>{application}</div>
            </Collapsible>
          }
        </div>
      
      </div>
      <div className="right-pdp">
        <section>
          <h1 className='pdp-h1'>{title}</h1>
          <article className='pdp-desc'>{prodDesc}</article>
        </section>
      </div>
    </div>
  );
};

PDPTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
  langKey: PropTypes.string
};

class PDP extends React.Component {
  render() {
    var dataMarkdown = [];
    let data;
    if (this.props.data !== null) {
      dataMarkdown = this.props.data.markdownRemark;
      data = this.props.data;
    }
    const jsonData = this.props.data.allArticlesJson.edges[0].node.articles;
    const { frontmatter } = data.markdownRemark;
    const { array, display } = frontmatter.slider;
    
    const langKey = frontmatter.lang;
    
    const title = frontmatter.title
    const prodDesc = frontmatter.prodDesc
    const application = frontmatter.application
    const specs = frontmatter.specs
    
    return (
      <Layout
        className="container"
        data={this.props.data}
        jsonData={jsonData}
        location={this.props.location}
      >
        <SEO frontmatter={frontmatter} />
        <div>
          <PDPTemplate
            contentComponent={HTMLContent}
            title={dataMarkdown.frontmatter.title}
            content={dataMarkdown.html}
            langKey={langKey}
            display={display}
            array={array}
            specs={specs}
            prodDesc={prodDesc}
            application={application}
          />
        </div>
      </Layout>
    );
  }
}

PDP.propTypes = {
  data: PropTypes.object.isRequired,
};

export default PDP;

export const pageQuery = graphql`
  query PDPQuery($id: String!) {
    site {
      siteMetadata {
        languages {
          defaultLangKey
          langs
        }
      }
    }
    allArticlesJson(filter: { title: { eq: "home" } }) {
      edges {
        node {
          articles {
            en
            sq
          }
        }
      }
    }
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        id
        title
        description
        lang
        prod1Title
        prodDesc
        application
        specs {
          spec
        }
        slider {
          display
          array {
            original
          }
        }
      }
      fields {
        slug
      }
    }
  }
`;
