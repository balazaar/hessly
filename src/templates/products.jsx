import React from 'react';
import * as PropTypes from 'prop-types';
import { graphql, Link } from 'gatsby';
import Layout from '../components/Layout';
import SEO from '../components/SEO/SEO';
import Content, { HTMLContent } from '../components/Content';
import menuTree from '../data/menuTree';
import select from '../components/utils';
import productLink from '../data/productsMenu'
import { FormattedMessage } from 'react-intl';

const ProductPageTemplate = ({
  title,
  content,
  contentComponent,
  langKey,
  cat1name,
  cat1desc,
  cat1image,
  cat2name,
  cat2desc,
  cat2image,
  cat3name,
  cat3desc,
  cat3image,
  cat4name,
  cat4desc,
  cat4image,
  cat5name,
  cat5desc,
  cat5image,
}) => {
  const PageContent = contentComponent || Content;
  const sel = select(langKey);

  return (
    <div className="container content service">
      <section className="section">
        <div className='services-flex'>
        <div className='column flex left'>
            <div className='column inner-container'>
              <h1>{cat1name}</h1>
              <p>{cat1desc}</p>
              <Link
                  className="hbtn"
                  to={productLink.interiorExterior[sel]}
                >
                  <FormattedMessage id='view-products'/>
              </Link>
            </div>
            <div
            className='column bg'
            style={{
              backgroundImage: `url(${!!cat1image.childImageSharp ? cat1image.childImageSharp.fluid.src : cat1image})`,
              backgroundPosition: `center`,
              backgroundAttachment: `scroll`,
              backgroundSize: `cover`,
              padding: '8rem'
            }}>
            </div>
          </div>
          <div className='column flex left'>
          <div
            className='column bg'
            style={{
              backgroundImage: `url(${cat2image})`,
              backgroundPosition: `center`,
              backgroundAttachment: `scroll`,
              backgroundSize: `cover`,
              padding: '8rem'
            }}>
            </div>
          <div className='column inner-container'>
              <h1>{cat2name}</h1>
              <p>{cat2desc}</p>
              <Link
                  className="hbtn"
                  to={productLink.drywallProfiles[sel]}
                >
                  <FormattedMessage id='view-products'/>
              </Link>
            </div>
          </div>

          <div className='column flex left'>
            <div className='column inner-container'>
              <h1>{cat3name}</h1>
              <p>{cat3desc}</p>
              <Link
                  className="hbtn"
                  to={productLink.steelRoofing[sel]}
                >
                  <FormattedMessage id='view-products'/>
              </Link>
            </div>
            <div
            className='column bg'
            style={{
              backgroundImage: `url(${cat3image})`,
              backgroundPosition: `center`,
              backgroundAttachment: `scroll`,
              backgroundSize: `cover`,
              padding: '8rem'
            }}>
            </div>
          </div>
          <div className='column flex left'>
            <div
            className='column bg'
            style={{
              backgroundImage: `url(${cat4image})`,
              backgroundPosition: `center`,
              backgroundAttachment: `scroll`,
              backgroundSize: `cover`,
              padding: '8rem'
            }}>
            </div>
            <div className='column inner-container'>
              <h1>{cat4name}</h1>
              <p>{cat4desc}</p>
              <Link
                  className="hbtn"
                  to={productLink.ceilingSystems[sel]}
                >
                  <FormattedMessage id='view-products'/>
              </Link>
            </div>
          </div>
          <div className='column flex left'>
            <div className='column inner-container'>
              <h1>{cat5name}</h1>
              <p>{cat5desc}</p>
              <Link
                  className="hbtn"
                  to={productLink.shelves[sel]}
                >
                  <FormattedMessage id='view-products'/>
              </Link>
            </div>
            <div
            className='column bg'
            style={{
              backgroundImage: `url(${cat5image})`,
              backgroundPosition: `center`,
              backgroundAttachment: `scroll`,
              backgroundSize: `cover`,
              padding: '8rem'
            }}>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

ProductPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
  langKey: PropTypes.string
};

class ProductPage extends React.Component {
  render() {
    var dataMarkdown = [];
    if (this.props.data !== null) {
      dataMarkdown = this.props.data.markdownRemark;
    }
    const jsonData = this.props.data.allArticlesJson.edges[0].node.articles;
    const { frontmatter } = dataMarkdown;

    const langKey = frontmatter.lang;

    const cat1name = frontmatter.cat1name
    const cat1image = frontmatter.cat1image.childImageSharp.fluid.src
    const cat1desc = frontmatter.cat1desc
    
    const cat2name = frontmatter.cat2name
    const cat2image = frontmatter.cat2image.childImageSharp.fluid.src
    const cat2desc = frontmatter.cat2desc

    const cat3name = frontmatter.cat3name
    const cat3image = frontmatter.cat3image.childImageSharp.fluid.src
    const cat3desc = frontmatter.cat3desc

    const cat4name = frontmatter.cat4name
    const cat4image = frontmatter.cat4image.childImageSharp.fluid.src
    const cat4desc = frontmatter.cat4desc

    const cat5name = frontmatter.cat5name
    const cat5image = frontmatter.cat5image.childImageSharp.fluid.src
    const cat5desc = frontmatter.cat5desc

    
    return (
      <Layout
        className="container"
        data={this.props.data}
        jsonData={jsonData}
        location={this.props.location}
      >
        <SEO frontmatter={frontmatter} />
        <div>
          <ProductPageTemplate
            contentComponent={HTMLContent}
            title={dataMarkdown.frontmatter.title}
            content={dataMarkdown.html}
            langKey={langKey}
            cat1desc={cat1desc}
            cat1image={cat1image}
            cat1name={cat1name}
            cat2desc={cat2desc}
            cat2image={cat2image}
            cat2name={cat2name}
            cat3desc={cat3desc}
            cat3image={cat3image}
            cat3name={cat3name}
            cat4desc={cat4desc}
            cat4image={cat4image}
            cat4name={cat4name}
            cat5desc={cat5desc}
            cat5image={cat5image}
            cat5name={cat5name}
          />
        </div>
      </Layout>
    );
  }
}

ProductPage.propTypes = {
  data: PropTypes.object.isRequired,
};

export default ProductPage;

export const pageQuery = graphql`
  query ProductPageQuery($id: String!) {
    site {
      siteMetadata {
        languages {
          defaultLangKey
          langs
        }
      }
    }
    allArticlesJson(filter: { title: { eq: "home" } }) {
      edges {
        node {
          articles {
            en
            sq
          }
        }
      }
    }
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        id
        title
        description
        lang
        cat1name
        cat1image {
            childImageSharp {
              fluid(maxWidth: 2048, quality: 100) {
                ...GatsbyImageSharpFluid
                src
              }
            }
          }
        cat1desc
        cat2name
        cat2image {
            childImageSharp {
              fluid(maxWidth: 2048, quality: 100) {
                ...GatsbyImageSharpFluid
                src
              }
            }
          }
        cat2desc
        cat3name
        cat3image {
            childImageSharp {
              fluid(maxWidth: 2048, quality: 100) {
                ...GatsbyImageSharpFluid
                src
              }
            }
          }
        cat3desc
        cat4name
        cat4image {
            childImageSharp {
              fluid(maxWidth: 2048, quality: 100) {
                ...GatsbyImageSharpFluid
                src
              }
            }
          }
        cat4desc
        cat5name
        cat5image {
            childImageSharp {
              fluid(maxWidth: 2048, quality: 100) {
                ...GatsbyImageSharpFluid
                src
              }
            }
          }
        cat5desc
      }
      fields {
        slug
      }
    }
  }
`;
