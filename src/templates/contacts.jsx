import React from 'react';
import * as PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import { navigate } from 'gatsby-link';
import Layout from '../components/Layout';
import SEO from '../components/SEO/SEO';
import Content, { HTMLContent } from '../components/Content';
import ContactDetails from '../components/ContactDetails';
import OsmMap from '../components/OsmMap';
import { getCurrentLangKey } from 'ptz-i18n';
import { FormattedMessage } from 'react-intl';

function encode(data) {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&');
}

function setActionPath(langKey) {
  let path;
  if (langKey === 'en') {
    path = '/en/contact/thanks/';
  } else {
    path = '/sq/kontakt/faleminderit/';
  }
  return path;
}

const ContactPageTemplate = ({
  title,
  content,
  contentComponent,
  image,
  address1,
  address2,
  phone1,
  phone2,
  loc1,
  loc2,
  handleSubmit,
  handleChange,
  action,
}) => {
  // const PageContent = contentComponent || Content;
  return (
    <div className="container contact">

      <section class="section">
        <div
          style={{
            backgroundImage: `url(${!!image.childImageSharp ? image.childImageSharp.fluid.src : image})`,
            backgroundPosition: 'center center',
            backgroundAttachment: 'scroll',
            backgroundSize: 'cover',
            backgroundColor: 'rgba(0,0,0,.5)',
            backgroundBlendMode: 'darken',
            backgroundRepeat: 'no-repeat'
          }}
          className="hero is-fullheight hero-contact">
          <div className='section-content'>
            <h1 style={{
              fontSize: '180%'
            }}>
              <FormattedMessage id='contact.contact-us' />
            </h1>
          </div>
        </div>
      </section>

      <div class="row align-center" id="row-813024510">
        <div class="col medium-10 small-12 large-10">
          <div class="col-inner">
            <div class="row" id="row-598215742">
              <div class="col medium-6 small-12 large-6">
                <div class="col-inner">
                  <div class="box has-hover has-hover box-text-bottom">
                    <div class="box-text text-center">
                      <div class="box-text-inner">
                        <h2><span>{loc1}</span></h2>
                        <p><span>{address1}</span></p>
                        <br />
                        <h4><span><FormattedMessage id='contact' />:</span></h4>
                        <p><span><a href={`tel://${phone1}`}>{phone1}</a></span></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col medium-6 small-12 large-6">
                <div class="col-inner">
                  <div class="box has-hover has-hover box-text-bottom">
                    <div class="box-text text-center">
                      <div class="box-text-inner">
                        <h2><span>{loc2}</span></h2>
                        <p><span>{address2}</span></p>
                        <br />
                        <h4><span><FormattedMessage id='contact' />:</span></h4>
                        <p><span><a href={`tel://${phone2}`} >{phone2}</a></span></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="form-content">
          <div className="box">
            <form
              name="contact"
              method="post"
              action={action}
              data-netlify="true"
              data-netlify-honeypot="bot-field"
              onSubmit={handleSubmit}
            >
              <input type="hidden" name="form-name" value="contact" />
              <div hidden>
                <label>
                  Don’t fill this out:{' '}
                  <input name="bot-field" onChange={handleChange} />
                </label>
              </div>
              <div className="field">
                <label className="label" htmlFor="name">
                  <FormattedMessage id="contact.name" />
                </label>
                <div className="control">
                  <input
                    className="input"
                    type="text"
                    name="name"
                    onChange={handleChange}
                    id="name"
                    required={true}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label" htmlFor="email">
                  <FormattedMessage id="contact.email" />
                </label>
                <div className="control">
                  <input
                    className="input"
                    type="email"
                    name="email"
                    onChange={handleChange}
                    id="email"
                    required={true}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label" htmlFor="company">
                  <FormattedMessage id="contact.company" />
                </label>
                <div className="control">
                  <input
                    className="input"
                    type="company"
                    name="company"
                    onChange={handleChange}
                    id="company"
                    required={false}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label" htmlFor="message">
                  <FormattedMessage id="contact.message" />
                </label>
                <div className="control">
                  <textarea
                    className="textarea"
                    name="message"
                    onChange={handleChange}
                    id="message"
                    required={true}
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button className="hbtn is-link" type="submit">
                    <FormattedMessage id="contact.send" />
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
    </div>
  );
};

ContactPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
};

class ContactPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isValidated: false };
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const form = e.target;
    fetch('/?no-cache=1', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({
        'form-name': form.getAttribute('name'),
        ...this.state,
      }),
    })
      .then(() => navigate(form.getAttribute('action')))
      .catch(error => alert(error));
  };
  render() {
    let dataMarkdown = [];
    let data;
    if (this.props.data !== null) {
      dataMarkdown = this.props.data.markdownRemark;
      data = this.props.data;
    }
    const location = this.props.location;
    const url = location.pathname;
    const { langs, defaultLangKey } = data.site.siteMetadata.languages;
    this.langKey = getCurrentLangKey(langs, defaultLangKey, url);
    const action = setActionPath(this.langKey);
    const jsonData = data.allArticlesJson.edges[0].node.articles;
    const address1 = dataMarkdown.frontmatter.address1;
    const address2 = dataMarkdown.frontmatter.address2;
    const phone1 = dataMarkdown.frontmatter.phone1;
    const phone2 = dataMarkdown.frontmatter.phone2;
    const loc1 = dataMarkdown.frontmatter.loc1;
    const loc2 = dataMarkdown.frontmatter.loc2;
    const locations = dataMarkdown.frontmatter.locations;
    const { lat } = locations;
    const { lng } = locations;
    const { message } = locations;
    const { frontmatter } = dataMarkdown;
    const imageSEO = frontmatter.image.childImageSharp.fluid.src
    return (
      <Layout
        className="container"
        data={data}
        jsonData={jsonData}
        location={location}
      >
        <SEO frontmatter={frontmatter} postImage={imageSEO} />
        <ContactPageTemplate
          contentComponent={HTMLContent}
          image={imageSEO}
          address1={address1}
          address2={address2}
          phone1={phone1}
          phone2={phone2}
          loc1={loc1}
          loc2={loc2}
          title={dataMarkdown.frontmatter.title}
          content={dataMarkdown.html}
          onSubmit={this.handleSubmit}
          action={action}
        />

        <OsmMap lat={lat} lng={lng} message={message} />
      </Layout>
    );
  }
}

ContactPage.propTypes = {
  data: PropTypes.object.isRequired,
};

export default ContactPage;

export const pageQuery = graphql`
  query ContactPageQuery($id: String!) {
    site {
      siteMetadata {
        languages {
          defaultLangKey
          langs
        }
      }
    }
    allArticlesJson(filter: { title: { eq: "home" } }) {
      edges {
        node {
          articles {
            en
            sq
          }
        }
      }
    }
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        id
        title
        description
        tags
        lang
        image {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
              src
            }
          }
        }
        address1
        address2
        phone1
        phone2
        loc1
        loc2
        locations {
          lat
          lng
          message
        }
      }
      fields {
        slug
      }
    }
  }
`;