---
id: '12'
title: 'Cement Board'
description: 'Cement Board page'
templateKey: productPage
lang: en
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }

prod1Title: 'Cement Board'
path: /en/products/interior-exterior/cement-board
slug: /en/products/interior-exterior/cement-board

specs:
    - { spec: 'Dimensions 1220*2440' }
    - { spec: 'Thickness: 11.5mm' }
    - { spec: 'Flexible' }
    - { spec: 'Easy to cut and work with' }
    - { spec: 'Very resistant to humidity and water' }

prodDesc: 'The cement board is a board manufactured from the joining of cement with fiberglass mesh. This kind of board is used to cover exterior and interior facades especially for its properties against humidity and water.'

---