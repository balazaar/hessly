---
id: '13'
title: 'Hessly Cement Board Joint Tape'
description: 'Hessly Cement Board Joint Tape page'
templateKey: productPage
lang: en
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }

prod1Title: 'Cement Board'
path: /en/products/interior-exterior/hessly-cement-board-joint-tape
slug: /en/products/interior-exterior/hessly-cement-board-joint-tape

specs:
    - { spec: 'Length: 50 m' }
    - { spec: 'Width: 150mm' }
    - { spec: 'Strong adhesion' }
   
prodDesc: 'The cement board joint tape is an accessory used for the joints of Cement Board. It is composed of alkali-resisting fiberglass.'

---