---
id: '14'
title: 'Hessly Fiberglass Mesh'
description: 'Hessly Fiberglass Mesh page'
templateKey: productPage
lang: en
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }

prod1Title: 'Cement Board'
path: /en/products/interior-exterior/hessly-fiberglass-mesh
slug: /en/products/interior-exterior/hessly-fiberglass-mesh
specs:
    - { spec: 'Length: 50 m' }
    - { spec: 'Width: 1m' }
   
prodDesc: 'Hessly Fiberglass Mesh is a kind of accessory product used in the exterior façade applications for construction reinforcing. This mesh is manufactured from woven fiberglass which is alkali-resistant.'

---