---
id: '02'
title: 'Products'
description: 'Product Category Page'
templateKey: products
lang: en
cat1name: 'Interior & Exterior'
cat1image: '/img/cat1image.jpeg'
cat1desc: 'Hessly introduces high quality products to consumers and businesses in interior and exterior construction of facades, walls and ceilings.'
cat2name: 'Drywall Profiles & Accessories'
cat2image: '/img/cat2image.jpeg'
cat2desc: 'Hessly steel profiles and accessories are the best solution a builder can choose in the construction of drywall systems. With more than 10-years of experience, we offer undisputable quality so that every client is more than assured in their choice.'
cat3name: 'Steel Roofing'
cat3image: '/img/cat3image.jpeg'
cat3desc: 'With over 10- years of experience in the steel sector, Hessly offers quick, affordable and long-lasting solutions for roofing and floor laying.'
cat4name: 'Hessly Ceiling Systems'
cat4image: '/img/cat4image.jpeg'
cat4desc: 'With more than 10 years of tradition, Hessly brings in the Albanian market only the highest quality of Ceiling Systems 600*600mm with variations that satisfy every customer’s needs for offices, commercial and residential zones.'
cat5name: 'Shelves & Accessories'
cat5image: '/img/cat5image.jpeg'
cat5desc: 'With more than 6 years of experience in the Albanian shelves market, Hessly provides high quality and long lasting products for fulfilling every shop need.'
path: /en/products/
slug: /en/products/
---