---
id: '11'
title: 'Hessly CW Channel'
description: 'Drywall Profiles and Accessories page'
templateKey: productPage
lang: en
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
prod1Title: 'Hessly CW Channel'
path: /en/products/drywall-profiles-accessories/hessly-cw-channel
slug: /en/products/drywall-profiles-accessories/hessly-cw-channel

specs:
    - { spec: 'Inner channel allows installation of electric wires' }
    - { spec: 'Two punched holes for easy installation of water and electric connections' }
    - { spec: 'Three channels on each side for better resistance' }
    - { spec: 'Standard length: 3m/ 4m' }
    - { spec: 'Different length dimensions upon request' }
    - { spec: 'Model: Normal / Bombardment' }

prodDesc: 'Hessly CW Channels are light galvanized steel sections. The galvanization process used for the raw material of these sections is 100 g/m2, which is the highest standard of galvanization used in the industry of Drywall Construction. This galvanization gives the steel a coating which protects it from rust and corrosion.'
application: 'Hessly CW Channels are light galvanized steel sections. The galvanization process used for the raw material of these sections is 100 g/m2, which is the highest standard of galvanization used in the industry of Drywall Construction. This galvanization gives the steel a coating which protects it from rust and corrosion.'

---

