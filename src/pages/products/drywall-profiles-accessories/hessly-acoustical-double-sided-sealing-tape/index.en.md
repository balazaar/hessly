---
id: '20'
title: 'Hessly Acoustical Double-sided Sealing Tape'
description: 'Hessly Acoustical Double-sided Sealing Tape page'
templateKey: productPage
lang: en
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
prod1Title: 'Hessly CW Channel'
path: /en/products/drywall-profiles-accessories/hessly-acoustical-double-sided-sealing-tape
slug: /en/products/drywall-profiles-accessories/hessly-acoustical-double-sided-sealing-tape

specs:
    - { spec: 'Flexible adhesion' }
    - { spec: 'High resistance' }
    - { spec: 'Acoustic protection' }
    - { spec: 'Different width size for partitions' }
    - { spec: '4mm thickness' }

prodDesc: 'Hessly Sealing Tape is a closed foam tape, self adhesive on both sides and flexible. It is recommended for the connection of flanking constructional components with the backside of the runners in drywall partitions.'
application: 'Application surfaces should be clean, dry and free from all contaminants like dust, rust, grease etc.'

---

