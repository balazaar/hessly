---
id: '19'
title: 'Hessly Fiberglass Joint Tape'
description: 'Hessly Fiberglass Joint Tape page'
templateKey: productPage
lang: en
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
prod1Title: 'Hessly CW Channel'
path: /en/products/drywall-profiles-accessories/hessly-fiberglass-joint-tape
slug: /en/products/drywall-profiles-accessories/hessly-fiberglass-joint-tape

specs:
    - { spec: 'Self adhesive Fibreglass Plasterboard Jointing & Repair Tape' }
    - { spec: 'Open fibreglass mesh eliminates blisters and bubbles' }
    - { spec: 'Requires no pre-plastering which also reduces drying time' }
    - { spec: 'Can be used for patch repair work' }
    - { spec: 'Mould-resistant' }

prodDesc: 'Strong, flexible self-adhesive fibreglass mesh tape for covering drywall joints prior to texturing and finishing.'
---