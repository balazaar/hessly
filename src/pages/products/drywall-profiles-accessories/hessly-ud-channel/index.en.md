---
id: '17'
title: 'Hessly UD Channel'
description: 'Hessly UD Channel page'
templateKey: productPage
lang: en
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
prod1Title: 'Hessly CW Channel'
path: /en/products/drywall-profiles-accessories/hessly-ud-channel
slug: /en/products/drywall-profiles-accessories/hessly-ud-channel

specs:
    - { spec: 'Standard length: 3m' }
    - { spec: 'Different length dimensions upon request' }
    - { spec: 'Model: Normal / Bombardment' }

prodDesc: 'Hessly UD Channels are light galvanized steel sections. The galvanization process used for the raw material of these sections is 100 g/m2, which is the highest standard of galvanization used in the industry of Drywall Construction. This galvanization gives the steel a coating which protects it from rust and corrosion.'
application: 'The UD Channel is used as a surrounding channel in the hanged ceilings applications as well as a vertical channel in the wall covering applications'

---

