---
id: '16'
title: 'Hessly CD Channel'
description: 'Hessly CD Channel page'
templateKey: productPage
lang: en
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
prod1Title: 'Hessly CW Channel'
path: /en/products/drywall-profiles-accessories/hessly-cd-channel
slug: /en/products/drywall-profiles-accessories/hessly-cd-channel

specs:
    - { spec: 'Different levels of steel thickness 0.4-0.6mm' }
    - { spec: 'Full range of accessories' }
    - { spec: 'Three outer side channels for better resistance' }
    - { spec: 'Standard Length: 3M/ 4M' }
    - { spec: 'Different lengths upon request' }
    - { spec: 'Model: Normal / Bombardment' }

prodDesc: 'Hessly CD Channels are light galvanized steel sections. The galvanization process used for the raw material of these sections is 100 g/m2, which is the highest standard of galvanization used in the industry of Drywall Construction. This galvanization gives the steel a coating which protects it from rust and corrosion.'
application: 'The CD Channel is the main channel in the construction of Drywall Systems. The width of this channel allows a quick and easy installation.'

---

