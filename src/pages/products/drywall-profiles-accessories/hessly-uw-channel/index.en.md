---
id: '15'
title: 'Hessly UW Channel'
description: 'Hessly UW Channel page'
templateKey: productPage
lang: en
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
prod1Title: 'Hessly CW Channel'
path: /en/products/drywall-profiles-accessories/hessly-uw-channel
slug: /en/products/drywall-profiles-accessories/hessly-uw-channel

specs:
    - { spec: 'Standard length: 3m' }
    - { spec: 'Different length dimensions upon request' }
    - { spec: 'Model: Normal / Bombardment' }

prodDesc: 'Hessly UW Channels are light galvanized steel sections. The galvanization process used for the raw material of these sections is 100 g/m2, which is the highest standard of galvanization used in the industry of Drywall Construction. This galvanization gives the steel a coating which protects it from rust and corrosion.'
application: 'These channels are used as surrounding sections in the lower parts of the vertical columns in order to complete the wall separation. The different widths in the inner channel allow for different dimensions of wall construction, particularly 50mm,75mm and 100mm. The UW channel is used in conjunction with the CW channel where the width is always corresponding.'

---

