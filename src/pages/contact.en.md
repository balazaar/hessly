---
id: '05'
title: 'Contact Us'
description: 'Page to contact us'
templateKey: contacts
tags:
  - web
lang: en
image: /img/Tirana.jpg
date: '08-03-2019'
loc1: 'Central Factory'
loc2: 'Sales Office'
address1: "Rruga 29 Nëntori, 1025, Tirana, Albania"
address2: "Rruga Abdyl Matoshi Nr. 2, 1025,Tirana, Albania"
phone1: '+355692038601'
phone2: '+355692502953'
locations:
  lat: 41.3409813
  lng: 19.7796741
  message: 'Hessly Sh.p.k'
path: /en/contact
slug: /en/contact
---