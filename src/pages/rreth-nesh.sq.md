---
id: '03'
title: 'Rreth nesh'
description: 'Rreth Hessly Sh.P.K'
templateKey: about
lang: sq
image: /img/PersimmonHD.jpg
date: '05-03-2019'
path: /sq/rreth-nesh/
slug: /sq/rreth-nesh/
---

Hessly Sh.P.K u themelua në vitin 2010 me veprimtari prodhuese dhe tregtuese në sektorin e materialeve të ndërtimit të thatë. Fillimet tona janë modeste por qëndrimi dhe profesionalizmi ynë na kanë ndihmuar për tu konsoliduar si një nga prodhuesit kryesorë të profileve të sistemit të thatë në tregun Shqiptar. Kjo këmbëngulje na ka dhënë gjithashtu mundësinë e zgjerimit në sektorë të tjerë të prodhimit dhe tregtimit. Hessly Sh.P.K, prej disa vitesh, ka zgjeruar kapacitetet e biznesit në prodhimin dhe tregtimin e mbulesave të lyera të çelikut dhe gjithashtu në sektorin e tregtimit të rafteve metalikë të supermarketeve dhe aksesorëve të fushës. Kapaciteti i sotëm prodhues i përpunimit të profileve të sistemit të thatë është 3000 ton/vit me një kapacitet të shtuar prej 1000 ton/vit në fushën e prodhimit të mbulesave të lyera të llamarinës. 

**Ne jemi krenarë për efiçencën që tregojmë në marrëdheniet tona me tregun e cila bëhet e mundur nga një staf tejet i kualifikuar në sektorin e prodhimit dhe menaxhimit. Tre vlerat tona kryesore në marrëdhëniet e përditshme të të bërit biznes janë: Cilësia, Efiçenca dhe Besueshmëria.**

- **Cilësia**: Hessly ka një historik të gjatë të përmbushjes së nevojave të klientëve sipas kërkesave specifike duke ruajtur gjithmonë një cilësi të qëndrueshme në sajë të ndjekjes së një etike themelore të të bërit biznes.

- **Efiçenca: Sigurimi i një procesi prodhimi të shpejtë i cili përmbush çdo kërkesë në kohë rekord.**

- **Besueshmëria**: Ngritja e marrëdhënieve afatgjata janë pjesë themelore e punës së Hessly. Ne jemi gjithmonë të hapur dhe tëpërkushtuar për të ofruar ndihmë dhe zgjidhur çdo problem, duke i ofruar çdo klienti një eksperiencë tërësisht profesionale.
Vizioni ynë ndërthuret me konsolidimin tonë si lider në secilin sektor që operojmë në tregun shqiptar, duke shpresuar gjithashtu, që në një të ardhme afat-gjatë të pozicionohemi si një nga furnitorët kryesorë në rajonin e Ballkanit.

**Vizioni ynë ndërthuret me konsolidimin tonë si lider në secilin sektor që operojmë në tregun shqiptar, duke shpresuar gjithashtu, që në një të ardhme afat-gjatë të pozicionohemi si një nga furnitorët kryesorë në rajonin e Ballkanit.**
