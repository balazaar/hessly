---
id: '01'
title: 'Partneri Juaj në Ndërtim'
description: 'Pershkrimi i kryefaqjes'
templateKey: home
tags:
  - web
lang: sq
date: '05-03-2019'
image: /img/hesslyhome.jpeg
heading: 'Zbulo më shumë'
slider:
  display: 'slide'
  array:
    - {
        original: 'https://picsum.photos/id/1015/6000/4000.jpg',
        thumbnail: 'https://picsum.photos/id/1015/6000/4000.jpg',
        originalAlt: 'A wild fjord in the far north.',
        originalTitle: 'A wild fjord in the far north.',
        description: 'A wild fjord in the far North, admiring the infinite.',
      }
    - {
        original: 'https://picsum.photos/id/1019/5472/3648.jpg',
        thumbnail: 'https://picsum.photos/id/1019/5472/3648.jpg',
        originalAlt: 'A beautiful sunset over the sea.',
        originalTitle: 'A beautiful sunset over the sea.',
        description: 'A beautiful sunset over the sea, where numerous routes of imagination depart.',
      }
    - {
        original: 'https://picsum.photos/id/1022/6000/3376.jpg',
        thumbnail: 'https://picsum.photos/id/1022/6000/3376.jpg',
        originalAlt: 'A Northern Lights.',
        originalTitle: 'A Northern Lights.',
        description: 'A northern lights with greenish hues.',
      }
mainpitch:
  heading: 'Artworks with augmented reality'
  subheading: 'Interactive Art'
  title: 'Why Kaki?'
  description: 'Because it is a fruit with incredible qualities...'
  link: /it/opere/nuovi-media/realtà-aumentata/
main:
  image1:
    alt: a persimmon
    image: /img/wildPersimmon.jpg
testimonials:
  - author: anonimous
    quote: >-
      The persimmon tree became "the tree of peace"
      because after Nagasaki destruction in august 1945,
      survived only some persimmon trees.
imageCardSL:
  alt: 'image'
  image: /img/128x128.png
  name: John Kaki
  description: I am a visual artist, follow me in this adventure...!
  website: www.kaki.com
path: /en/
slug: /en/
---
