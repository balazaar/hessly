---
id: '11'
title: 'Hessly Profil CW'
description: 'Drywall Profiles and Accessories page'
templateKey: productPage
lang: sq
slider: 
    display: 'slide'
    array:
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
    - { original: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=0.752xw:1.00xh;0.175xw,0&resize=640:*' }
prod1Title: 'Hessly CW Channel'
path: /sq/produkte/profile-aksesore/hessly-profil-cw
slug: /sq/produkte/profile-aksesore/hessly-profil-cw

specs:
    - { spec: 'Kanal i brendshëm për shtrirjen e lidhjeve elektrike' }
    - { spec: 'Dy vrima për futjen e lidhjeve të ujit dhe ato elektrike' }
    - { spec: 'Tre kanale të jashtëm në secilën faqe anësore për një qëndrueshmëri më të fortë të profilit.' }
    - { spec: 'Gjatesi standarte: 3m/ 4m' }
    - { spec: 'Gjatësi të ndryshme në bazë të kërkesës' }
    - { spec: 'Stili: Bombardim / Normal' }

prodDesc: 'Profilet CW Hessly janë profile të lehta të prodhuara nga çeliku i galvanizuar. Galvanizimi i përdorur në këto profile është 100 g/m2 i cili është standarti me i lartë i industrisë së strukturës së sistemit të thatë. Ky galvanizim i lartë i jep profilit një veshje e cila e mbron atë nga grryerja dhe formimi i ndryshkut.'
application: 'Këto profile përdoren si kolona vertikale në ndërtimin e mureve dhe ndarjeve në sistemet e thata të gipsit. Gjerësitë e ndryshme të pjesës së brendshme të profilit lejojnë ndërtimin e mureve dhe ndarjeve me hapësirë të ndryshme, 50mm, 75mm dhe 100mm. Profili CW mund të përdoret edhe si profil mbajtës i tavaneve të varura, i pafiksuar në soletë. Profili CW përdoret në ndërthurje me profilin UW dhe gjithmonë gjerësia e tij duhet të përkojë në ndërtim më gjerësinë e profilit UW.'

---

