---
id: '03'
title: 'About us'
description: 'About Hessly'
templateKey: about
lang: en
image: /img/PersimmonHD.jpg
date: '05-03-2019'
path: /en/about/
slug: /en/about/
---

Hessly Sh.P.K was established in 2010 primarily as a manufacturing and trading company in the sector of Drywall Building Materials. Our beginnings are humble and our professional attitude has helped us to become one of the leading manufacturers of galvanized steel keels in the Albanian domestic market. At the moment, the company boasts manufacturing capacities of 3000 tons of galvanized steel. This attitude has pushed us further into different markets in different time milestones. Hessly has expanded its business operations in the market of Steel Roofing production and trading and the Supermarket Shelving and Accessories sector.

**We pride ourselves as one of the most efficient manufacturers in the market by employing professionals that provide us with our core values: Quality, Efficiency, Reliability.**

- **Quality**: Hessly has a long history of fulfilling clients' needs according to specific requirements by always reassuring a constant satisfying product quality. This is done following a strict business ethic where the customer's satisfaction is of paragon importance. 

- **Efficiency**: Reassuring a production process that fulfills demands in record times.

- **Reliability**: The establishment of long-term business relationships are at the center of our doctrine of doing business. We have always been willing and attentive in our efforts to help our clients with every problem faced to ensure a customer experience like no other competitor in the industry. 

**Our vision is to become leaders in each and every sector we are operating in the Albanian domestic market, as well as establishing ourselves as a main supplier in the long-term future for the region.**

